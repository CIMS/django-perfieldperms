#!/usr/bin/env python

import os, sys, glob

import django
from django.conf import settings
from django.core.management import execute_from_command_line


DEFAULT_SETTINGS = dict(
    INSTALLED_APPS=(
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'perfieldperms.apps.PerfieldpermsConfig',
        'tests',
        ),
    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
        ],
    DATABASES={
        "default": {
            'ENGINE': 'django.db.backends.sqlite3',
            }
        },
    SILENCED_SYSTEM_CHECKS=["1_7.W001"],
    # Use a fast hasher to speed up tests.
    PASSWORD_HASHERS=(
        'django.contrib.auth.hashers.MD5PasswordHasher',
        ),
    ROOT_URLCONF='tests.urls',
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                    ],
                },
            },
        ],
    STATIC_URL = '/static/',
    )


def runtests():
    if not settings.configured:
        settings.configure(**DEFAULT_SETTINGS)

    # Compatibility with Django 1.7's stricter initialization
    if hasattr(django, 'setup'):
        django.setup()

    parent = os.path.dirname(os.path.abspath(__file__))
    sys.path.insert(0, parent)

    test_args = []
    try:
        from django.test.runner import DiscoverRunner
        runner_class = DiscoverRunner
        test_path = 'tests'
    except ImportError:
        from django.test.simple import DjangoTestSuiteRunner
        runner_class = DjangoTestSuiteRunner
        test_path = 'tests'

    # Allow accessing test options from the command line.
    try:
        test_path = test_path + '.' + sys.argv[1]
    except IndexError:
        pass
    
    test_args.append(test_path)
    failures = runner_class(
            verbosity=1, interactive=True, failfast=False).run_tests(test_args)
    sys.exit(failures)


if __name__ == '__main__':
    runtests()
