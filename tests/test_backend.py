from django.test import TestCase
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AnonymousUser, User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.management import call_command
from django.utils.six import StringIO

from perfieldperms.backends import PFPBackend
from perfieldperms.models import PerFieldPermission, PFPContentType
from perfieldperms.utils import list_fields_for_ctype
from tests import helpers


# A lot if these need to be split out into individual tests.
class TestPFPBackend(TestCase):

    @classmethod
    def setUpTestData(cls):
        pwd = make_password('password')
        u1 = User(username='u1', password=pwd)
        u2 = User(username='u2', password=pwd)
        u3 = User(username='u3', password=pwd)
        u4 = User(username='u4', password=pwd)
        u5 = User(username='u5', password=pwd)
        u6 = User(username='u6', password=pwd)
        u1.save()
        u2.save()
        u3.save()
        u4.save()
        u5.save()
        u6.save()
        g1 = Group(name='g1')
        g2 = Group(name='g2')
        g3 = Group(name='g3')
        g4 = Group(name='g4')
        g1.save()
        g2.save()
        g3.save()
        g4.save()

    def test_get_user_permissions(self):
        """Empty set is empty."""
        u1 = User.objects.get(username='u1')
        chk_set = set()
        backend = PFPBackend()
        self.assertSetEqual(chk_set, backend.get_user_permissions(u1))

    def test_get_user_permissions_caching(self):
        """Cache is used once populated."""
        u1 = User.objects.get(username='u1')
        chk_set = set()
        backend = PFPBackend()
        perm = helpers.get_perm('auth', 'user', 'add_user')
        backend.get_user_permissions(u1)
        u1.user_permissions.add(perm)
        self.assertSetEqual(chk_set, backend.get_user_permissions(u1))

    def test_get_user_permissions_inactive(self):
        """Inactive user has no permissions."""
        u1 = User.objects.get(username='u1')
        chk_set = set()
        backend = PFPBackend()
        perm = helpers.get_perm('auth', 'user', 'add_user')
        u1 = User.objects.get(username='u1')
        u1.user_permissions.add(perm)
        u1.is_active = False
        self.assertSetEqual(chk_set, backend.get_user_permissions(u1))

    def test_get_user_permissions_anon(self):
        """Anonymous user has no permissions."""
        chk_set = set()
        backend = PFPBackend()
        u1 = AnonymousUser()
        self.assertSetEqual(chk_set, backend.get_user_permissions(u1))

    def test_get_user_permissions_hasperms(self):
        """Return permissions when present."""
        u2 = User.objects.get(username='u2')
        chk_set = set()
        backend = PFPBackend()
        perm = helpers.get_perm('auth', 'user', 'add_user')
        u2.user_permissions.add(perm)
        chk_set.add(helpers.get_perm_str(perm))
        perm = helpers.get_perm('auth', 'user', 'change_user')
        chk_set.add(helpers.get_perm_str(perm))
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        u2.user_permissions.add(perm)
        chk_set.add(helpers.get_perm_str(perm))
        self.assertSetEqual(chk_set, backend.get_user_permissions(u2))

    def test_get_user_permissions_superuser(self):
        """Superuser is given all permissions."""
        u3 = User.objects.get(username='u3')
        backend = PFPBackend()
        u3.is_superuser = True
        perms = Permission.objects.filter(
                perfieldpermission__isnull=True,
                ).values_list(
                'content_type__app_label',
                'codename',
                )
        chk_set = set("{}.{}".format(ct, name) for ct, name in perms)
        for perm in chk_set:
            with self.subTest(perm=perm):
                self.assertTrue(perm in backend.get_user_permissions(u3))

    def test_get_group_permissions(self):
        """Same sequence of tests as for user above."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        u4 = User.objects.get(username='u4')
        g1 = Group.objects.get(name='g1')
        g2 = Group.objects.get(name='g2')
        chk_set = set()
        backend = PFPBackend()
        # Check empty permissions
        self.assertSetEqual(chk_set, backend.get_group_permissions(u1))
        # Check user with User ctype perms
        perm = helpers.get_perm('auth', 'user', 'add_user')
        u2.user_permissions.add(perm)
        self.assertSetEqual(chk_set, backend.get_group_permissions(u2))
        # Check caching
        u2.groups.add(g1)
        g1.permissions.add(perm)
        self.assertSetEqual(chk_set, backend.get_group_permissions(u2))
        # Gets set perms
        g2.permissions.add(perm)
        chk_set.add(helpers.get_perm_str(perm))
        perm = helpers.get_perm('auth', 'user', 'change_user')
        chk_set.add(helpers.get_perm_str(perm))
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        g2.permissions.add(perm)
        chk_set.add(helpers.get_perm_str(perm))
        u3.groups.add(g2)
        self.assertSetEqual(chk_set, backend.get_group_permissions(u3))
        # Check superuser
        u4.is_superuser = True
        perms = Permission.objects.filter(
                perfieldpermission__isnull=True,
                ).values_list(
                'content_type__app_label',
                'codename',
                )
        chk_set = set("{}.{}".format(ct, name) for ct, name in perms)
        for perm in chk_set:
            with self.subTest(perm=perm):
                self.assertTrue(perm in backend.get_group_permissions(u4))

    def test_get_all_permissions(self):
        """Largely same set of tests as for user."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        u4 = User.objects.get(username='u4')
        g1 = Group.objects.get(name='g1')
        g2 = Group.objects.get(name='g2')
        chk_set = set()
        backend = PFPBackend()
        # Check no perms
        self.assertSetEqual(chk_set, backend.get_all_permissions(u1))
        # Check caching
        perm = helpers.get_perm('auth', 'user', 'add_user')
        u1.user_permissions.add(perm)
        self.assertSetEqual(chk_set, backend.get_all_permissions(u1))
        # Check override of group perms
        perm = helpers.get_perm('auth', 'user', 'change_user')
        u2.user_permissions.add(perm)
        chk_set.add(helpers.get_perm_str(perm))
        u2.groups.add(g1)
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        g1.permissions.add(perm)
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'groups')
        g1.permissions.add(perm)
        self.assertSetEqual(chk_set, backend.get_all_permissions(u2))
        # Check merge of user/group perms
        #perm = helpers.get_perm('auth', 'user', 'change_user')
        #u3.user_permissions.add(perm)
        #chk_set.add(helpers.get_perm_str(perm))
        #u3.groups.add(g2)
        #perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        #g2.permissions.add(perm)
        #perm = helpers.create_pfp('auth', 'user', 'change_user', 'groups')
        #g2.permissions.add(perm)
        #perm = helpers.create_pfp('auth', 'user', 'add_user', 'is_staff')
        #g2.permissions.add(perm)
        #chk_set.add(helpers.get_perm_str(perm))
        #perm = helpers.create_pfp('auth', 'user', 'add_user', 'groups')
        #u3.user_permissions.add(perm)
        #chk_set.add(helpers.get_perm_str(perm))
        #chk_set.add('auth.add_user')
        #self.assertSetEqual(chk_set, backend.get_all_permissions(u3))
        # Check superuser
        u4.is_superuser = True
        perms = Permission.objects.filter(
                perfieldpermission__isnull=True,
                ).values_list(
                'content_type__app_label',
                'codename',
                )
        chk_set = set("{}.{}".format(ct, name) for ct, name in perms)
        for perm in chk_set:
            with self.subTest(perm=perm):
                self.assertTrue(perm in backend.get_all_permissions(u4))

    def test_has_perm(self):
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        u4 = User.objects.get(username='u4')
        u5 = User.objects.get(username='u5')
        u6 = User.objects.get(username='u6')
        g1 = Group.objects.get(name='g1')
        g2 = Group.objects.get(name='g2')
        backend = PFPBackend()
        # Check no perms
        perm = Permission.objects.first()
        perm = '{}.{}'.format(perm.content_type.app_label, perm.codename)
        self.assertFalse(backend.has_perm(u1, perm))
        # Check caching
        perm = helpers.get_perm('auth', 'user', 'add_user')
        perm_str = '{}.{}'.format(perm.content_type.app_label, perm.codename)
        u1.user_permissions.add(perm)
        self.assertFalse(backend.has_perm(u1, perm_str))
        # Check has user perm
        u2.user_permissions.add(perm)
        self.assertTrue(backend.has_perm(u2, perm_str))
        # Inactive user has no perms
        u2 = User.objects.get(username='u2')
        u2.is_active = False
        self.assertFalse(backend.has_perm(u2, perm_str))
        # Anonymous user has no perms
        u2 = AnonymousUser()
        u2.is_active = False
        self.assertFalse(backend.has_perm(u2, perm_str))
        # Check has group perm
        g1.permissions.add(perm)
        u3.groups.add(g1)
        self.assertTrue(backend.has_perm(u3, perm_str))
        # Check override of group perms. User has model level perm, group has
        # some field level. Check of a field level perm the group doesn't have
        # should return True
        perm = helpers.get_perm('auth', 'user', 'change_user')
        u4.user_permissions.add(perm)
        u4.groups.add(g2)
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        g2.permissions.add(perm)
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'groups')
        perm_str = '{}.{}'.format(perm.content_type.app_label, perm.codename)
        g2.permissions.add(perm)
        self.assertTrue(backend.has_perm(u4, perm_str))
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'email')
        perm_str = '{}.{}'.format(perm.content_type.app_label, perm.codename)
        self.assertTrue(backend.has_perm(u4, perm_str))
        # Check model level access if user has field permission
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        u6.user_permissions.add(perm)
        perm_str = helpers.get_perm_str(helpers.get_perm('auth', 'user', 'change_user'))
        self.assertTrue(backend.has_perm(u6, perm_str))
        # Check Superuser has all the perms.
        u5.is_superuser = True
        perms = Permission.objects.all().values_list(
                'content_type__app_label',
                'codename',
                )
        perms = ["{}.{}".format(ct, name) for ct, name in perms]
        for perm in perms:
            with self.subTest(perm=perm):
                self.assertTrue(backend.has_perm(u5, perm))

    def test_has_module_perms_noperms(self):
        """User with no permissions has no module permissions."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        backend = PFPBackend()
        perm = helpers.get_perm('auth', 'user', 'add_user')
        app_label = perm.content_type.app_label
        self.assertFalse(backend.has_module_perms(u1, app_label))

    def test_has_module_perms_caching(self):
        """Check results are returned from cache."""
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        backend = PFPBackend()
        perm = helpers.get_perm('auth', 'user', 'add_user')
        app_label = perm.content_type.app_label
        backend.has_module_perms(u1, app_label)
        u1.user_permissions.add(perm)
        self.assertFalse(backend.has_module_perms(u1, app_label))

    def test_has_module_perms_hasperms(self):
        """Returns true when user has permission."""
        u2 = User.objects.get(username='u2')
        backend = PFPBackend()
        perm = helpers.get_perm('auth', 'user', 'add_user')
        app_label = perm.content_type.app_label
        u2.user_permissions.add(perm)
        self.assertTrue(backend.has_module_perms(u2, app_label))

    def test_has_all_fields(self):
        u1 = User.objects.get(username='u1')
        u2 = User.objects.get(username='u2')
        u3 = User.objects.get(username='u3')
        u4 = User.objects.get(username='u4')
        g1 = Group.objects.get(name='g1')
        g2 = Group.objects.get(name='g2')
        backend = PFPBackend()
        # User has field level perms
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        u1.user_permissions.add(perm)
        self.assertFalse(backend.has_all_fields(u1, perm))
        # User has model level perm
        perm = helpers.get_perm('auth', 'user', 'change_user')
        u2.user_permissions.add(perm)
        self.assertTrue(backend.has_all_fields(u2, perm))
        # User has field level perms and pass permission string
        perm = helpers.create_pfp('auth', 'user', 'change_user', 'is_staff')
        perm_str = '{}.{}'.format(perm.content_type.app_label, perm.codename)
        u3.user_permissions.add(perm)
        self.assertFalse(backend.has_all_fields(u3, perm_str))


