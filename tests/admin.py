from django.contrib import admin
from perfieldperms.admin import PFPModelAdmin

from tests.models import Pizza


admin.site.register(Pizza, PFPModelAdmin)
