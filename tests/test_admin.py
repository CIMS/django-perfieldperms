from django.test import TestCase, override_settings, RequestFactory, Client
from django.contrib.admin.sites import AdminSite
from django.contrib.auth.models import Permission, User, Group
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.core.exceptions import ValidationError
from django.template.response import TemplateResponse
from django.urls import reverse
from perfieldperms import admin
from perfieldperms.backends import PFPBackend
from perfieldperms.models import PerFieldPermission, PFPRoleType
from tests import helpers
from tests.models import NoRelToPermission, MultiRelToPermission, Pizza


@override_settings(
        AUTHENTICATION_BACKENDS=['perfieldperms.backends.PFPBackend'],
        PFP_MODELS=[('tests', 'pizza')],
        PFP_IGNORE_PERMS={}
        )
class PFPModelAdminTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')
        cls.adminsite = AdminSite()
        cls.superuser = User.objects.create_superuser(
                username='superfoo',
                password='supersekret',
                email='superfoo@example.com',
                )

    def setUp(self):
        self.client.force_login(self.superuser)

    def test_get_form_add(self):
        """Overriden get_form() add smoke test."""
        response = self.client.get(reverse('admin:tests_pizza_add'))
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(200, response.status_code)

    def test_get_form_change(self):
        """Overriden get_form() change smoke test."""
        pizza = Pizza.objects.create(name='Capriciosa', price='12.95')
        response = self.client.get(reverse('admin:tests_pizza_change', args=[pizza.pk]))
        self.assertIsInstance(response, TemplateResponse)
        self.assertEqual(200, response.status_code)


@override_settings(
        PFP_MODELS=[('auth', 'user')],
        PFP_IGNORE_PERMS={'auth': {'user': ['add_user']}}
        )
class PerFieldPermissionAdminTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.adminsite = AdminSite()
        call_command('pfp-makeperms')

    def setUp(self):
        self.madmin = admin.PerFieldPermissionAdmin(PerFieldPermission, self.adminsite)
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
                username='foo',
                password='supersekret',
                is_staff=True,
                is_superuser=True,
                )
        self.user_bar = User.objects.create_user(
                username='bar',
                password='supersekret',
                first_name='Bar',
                last_name='Baa',
                is_staff=True,
                )

    def test_get_filter_form_initial_empty(self):
        """A request with no parameter creates an empty initial dict."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        request.user = self.user
        initial = self.madmin._get_filter_form_initial(request)
        self.assertFalse(bool(initial))

    def test_get_filter_form_initial_get(self):
        """Request with GET parameters are correctly copied to initial dict."""
        user_ctype = ContentType.objects.get_for_model(User)
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/',
                {
                    'pfp_pfilter-model': user_ctype.id,
                    'pfp_pfilter-show_pfps': '1',
                    'pfp_rfilter-role_type': user_ctype.id
                }
                )
        request.user = self.user
        initial = self.madmin._get_filter_form_initial(request)
        self.assertEqual(user_ctype, initial['model'])
        self.assertEqual(user_ctype, initial['role_type'])
        self.assertTrue(initial['show_pfps'])

    def test_get_filter_form_initial_junk(self):
        """Junk in correctly named parameter is ignored."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/',
                {'pfp_pfilter-model': '1000', 'pfp_rfilter-role_type': '555'}
                )
        request.user = self.user
        initial = self.madmin._get_filter_form_initial(request)
        self.assertFalse(bool(initial))

    def test_get_perm_choices(self):
        """No filters gives unfiltered queryset, no pfps."""
        user_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=user_ctype,
                role_type=user_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=False
                )
        perms_qs = self.madmin._get_perm_choices(clean_data)
        test_qs = Permission.objects.filter(
                content_type=user_ctype,
                perfieldpermission__isnull=True,
                )
        self.assertSetEqual(set(test_qs), set(perms_qs))

    def test_get_perm_choices_show_pfps(self):
        """PFPs enabled gives all permissions."""
        user_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=user_ctype,
                role_type=user_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = self.madmin._get_perm_choices(clean_data)
        test_qs = Permission.objects.filter(
                    content_type=user_ctype)
        self.assertSetEqual(set(test_qs), set(perms_qs))

    def test_get_perm_choices_permissions(self):
        """Individual permissions selected, no fields. Expect permission set to
        contain only the selected permissions."""
        user_ctype = ContentType.objects.get_for_model(User)
        clean_perms = Permission.objects.filter(
                content_type=user_ctype,
                codename__in=['change_user__email', 'change_user__is_staff',
                              'change_user__is_active'],
                )
        clean_data = dict(
                model=user_ctype,
                role_type=user_ctype,
                permissions=clean_perms,
                model_fields=[],
                show_pfps=True
                )
        perms_qs = self.madmin._get_perm_choices(clean_data)
        self.assertSetEqual(set(clean_perms), set(perms_qs))

    def test_get_perm_choices_fields_only(self):
        """Fields selected, no permissions, not show_pfps. Expect a set with
        all pfps from the right ContentType and field_name in the model_fields
        list.
        """
        fields = ['email', 'is_staff', 'is_active']
        user_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=user_ctype,
                role_type=user_ctype,
                permissions=[],
                model_fields=fields,
                show_pfps=False
                )
        perms_qs = self.madmin._get_perm_choices(clean_data)
        test_qs = Permission.objects.filter(
                content_type=user_ctype,
                perfieldpermission__field_name__in=fields,
                )
        self.assertSetEqual(set(test_qs), set(perms_qs))
        self.assertEqual(
                len(Permission.objects.filter(
                    content_type=user_ctype,
                    perfieldpermission__isnull=True,
                    field_permissions__isnull=False,
                    ).distinct()
                    ) * len(fields),
                len(perms_qs)
                )

    def test_get_perm_choices_fields_and_perms(self):
        """
        Both fields and permissions selected. Expect result set to include
        selected PFPs, and PFPs for the fields selected IF a corresponding
        model level permission is also selected.
        """
        fields = ['email']
        user_ctype = ContentType.objects.get_for_model(User)
        clean_perms = Permission.objects.filter(
                content_type=user_ctype,
                codename__in=['change_user__is_staff', 'change_user'],
                )
        clean_data = dict(
                model=user_ctype,
                role_type=user_ctype,
                permissions=clean_perms,
                model_fields=fields,
                show_pfps=True
                )
        perms_qs = self.madmin._get_perm_choices(clean_data)
        test_qs = Permission.objects.filter(
                content_type=user_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        self.assertSetEqual(set(test_qs), set(perms_qs))

    def test_get_perm_select_init_no_roles(self):
        """No objects of the selected role type exist."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        role_ctype = ContentType.objects.get_for_model(Group)
        clean_data = dict(
                model=role_ctype,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        initial, page = self.madmin._get_perm_select_initial(request, clean_data, perms_qs)
        self.assertFalse(initial)

    def test_get_perm_select_init_print_username(self):
        """Role type is active user model, prints username."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        role_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=role_ctype,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        initial, page = self.madmin._get_perm_select_initial(request, clean_data, perms_qs)
        self.assertEqual(self.user.username, initial[0]['role_name'])

    def test_get_perm_select_init_print_fullname(self):
        """Role type is activer user model, print fullname if it exists."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        role_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=role_ctype,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        initial, page = self.madmin._get_perm_select_initial(request, clean_data, perms_qs)
        self.assertEqual(self.user_bar.get_full_name(), initial[1]['role_name'])

    def test_get_perm_select_init_users_noperms(self):
        """User does not possess selected perms, they are not set in the
        returned `initial` tuple.
        """
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        role_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=role_ctype,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        initial, page = self.madmin._get_perm_select_initial(request, clean_data, perms_qs)
        self.assertEqual(2, len(initial))
        for entry in initial:
            with self.subTest(uname=entry['role_name']):
                self.assertFalse(entry['permissions'])

    def test_get_perm_select_init_users(self):
        """
        User has an existing permission, it is set in the returned `initial`
        tuple.
        """
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        perm = helpers.get_perm('auth', 'user', 'change_user__email')
        self.user_bar.user_permissions.add(perm)
        role_ctype = ContentType.objects.get_for_model(User)
        clean_data = dict(
                model=role_ctype,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        test_perms = Permission.objects.filter(id=perm.id)
        initial, page = self.madmin._get_perm_select_initial(request, clean_data, perms_qs)
        self.assertSetEqual(
                set(test_perms),
                set(initial[1]['permissions'])
                )

    def test_get_perm_select_init_other_role_type(self):
        """Works with a role type other than the user model."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        perm = helpers.get_perm('auth', 'user', 'change_user__email')
        model_ctype = ContentType.objects.get_for_model(User)
        role_ctype = ContentType.objects.get_for_model(Group)
        g1 = Group.objects.create(name='g1')
        g1.permissions.add(perm)
        clean_data = dict(
                model=model_ctype,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=model_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        test_perms = Permission.objects.filter(id=perm.id)
        initial, page = self.madmin._get_perm_select_initial(request, clean_data, perms_qs)
        self.assertEqual(g1.name, initial[0]['role_name'])
        self.assertSetEqual(
                set(test_perms),
                set(initial[0]['permissions'])
                )

    def test_get_perm_select_init_role_no_rel_perms(self):
        """Exception raised if the role type has no relation to Permissions."""
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        perm = helpers.get_perm('auth', 'user', 'change_user__email')
        role_ctype = ContentType.objects.get_for_model(NoRelToPermission)
        NoRelToPermission.objects.create(name='nr1')
        clean_data = dict(
                model=perm.content_type,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        self.assertRaises(
                ValidationError,
                self.madmin._get_perm_select_initial,
                *[request, clean_data, perms_qs]
                )

    def test_get_perm_select_init_role_multi_rel_perms(self):
        """Exception raised if the role type has multiple relations to
        Permissions.
        """
        request = self.factory.get(
                '/admin/perfieldperms/perfieldpermission/manage/')
        perm = helpers.get_perm('auth', 'user', 'change_user__email')
        role_ctype = ContentType.objects.get_for_model(MultiRelToPermission)
        mr1 = MultiRelToPermission.objects.create(name='mr1')
        mr1.rel1.add(perm)
        mr1.rel2.add(perm)
        clean_data = dict(
                model=perm.content_type,
                role_type=role_ctype,
                permissions=[],
                model_fields=[],
                show_pfps=True
                )
        perms_qs = Permission.objects.filter(
                content_type=role_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        self.assertRaises(
                ValidationError,
                self.madmin._get_perm_select_initial,
                *[request, clean_data, perms_qs]
                )

    def test_process_perm_form_no_forms(self):
        """Empty forms process fine."""
        client = Client()
        client.login(username='foo', password='supersekret')
        model_ctype = ContentType.objects.get_for_model(User)
        role_ctype = ContentType.objects.get_for_model(Group)
        PFPRoleType.objects.create(role_ctype=role_ctype)
        perms_qs = Permission.objects.filter(
                content_type=model_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                ).values_list('id', flat=True)
        response = client.post(
                '/admin/perfieldperms/perfieldpermission/manage/',
                data={
                   'pfp_pfilter-model': model_ctype.id,
                   'pfp_filter-role_type': role_ctype.id,
                   'pfp_pfilter-permissions': list(perms_qs),
                   'pfp_pfilter-model_fields': [],
                   'pfp_rpfilter-show_pfps': 'on',
                   'form-TOTAL_FORMS': '0',
                   'form-INITIAL_FORMS': '0',
                   'form-MIN_NUM_FORMS': '0',
                   'form-MAX_NUM_FORMS': '0',
                },
                follow=True,
                )
        self.assertEqual(response.status_code, 200)

    def test_process_perm_form_role_id_changed(self):
        """
        Role ID doesn't match up to what is retrieved from the database, so
        role is skipped.
        """
        client = Client()
        client.login(username='foo', password='supersekret')
        model_ctype = ContentType.objects.get_for_model(User)
        role_ctype = ContentType.objects.get_for_model(User)
        perms_qs = Permission.objects.filter(
                content_type=model_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        self.user.user_permissions.add(perms_qs[0])
        perms_qs = [perm.id for perm in perms_qs]
        response = client.post(
                '/admin/perfieldperms/perfieldpermission/manage/',
                data={
                   'pfp_pfilter-model': model_ctype.id,
                   'pfp_pfilter-show_pfps': 'on',
                   'pfp_pfilter-permissions': list(perms_qs),
                   'pfp_pfilter-model_fields': [],
                   'pfp_rfilter-role_type': role_ctype.id,
                   'form-TOTAL_FORMS': '2',
                   'form-INITIAL_FORMS': '2',
                   'form-MIN_NUM_FORMS': '0',
                   'form-MAX_NUM_FORMS': '0',
                   'form-0-role_name': 'foo',
                   'form-0-role_id': '1',
                   'form-0-permissions': perms_qs[0],
                   'form-1-role_name': 'Testman Test',
                   'form-1-role_id': '5',
                   'form-1-permissions': perms_qs[1],
                   '_save': '',
                },
                follow=True,
                )
        self.assertContains(response, 'were not updated due to database changes')

    def test_process_perm_form_add_perms(self):
        "Permissions are added/removed properly."""
        client = Client()
        client.login(username='foo', password='supersekret')
        model_ctype = ContentType.objects.get_for_model(User)
        role_ctype = ContentType.objects.get_for_model(User)
        perms_qs = Permission.objects.filter(
                content_type=model_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        test_qs = perms_qs
        self.user.user_permissions.add(perms_qs[0])
        self.user_bar.user_permissions.add(perms_qs[0])
        perms_qs = [perm.id for perm in perms_qs]
        response = client.post(
                '/admin/perfieldperms/perfieldpermission/manage/',
                data={
                   'pfp_pfilter-model': model_ctype.id,
                   'pfp_pfilter-show_pfps': 'on',
                   'pfp_pfilter-permissions': list(perms_qs),
                   'pfp_pfilter-model_fields': [],
                   'pfp_rfilter-role_type': role_ctype.id,
                   'form-TOTAL_FORMS': '2',
                   'form-INITIAL_FORMS': '2',
                   'form-MIN_NUM_FORMS': '0',
                   'form-MAX_NUM_FORMS': '0',
                   'form-0-role_name': 'foo',
                   'form-0-role_id': '1',
                   'form-0-permissions': perms_qs[0],
                   'form-1-role_name': 'Testman Test',
                   'form-1-role_id': '2',
                   'form-1-permissions': perms_qs[1],
                   '_save': '',
                },
                follow=True,
                )
        self.assertNotContains(response, 'Permissions for foo')
        self.assertContains(response, 'Permissions for Testman Test')
        self.assertContains(response, 'Added: 1')
        self.assertContains(response, 'Removed: 1')
        self.assertSetEqual(
                set([test_qs[1]]),
                set(self.user_bar.user_permissions.all()),
                )

    def test_process_perm_form_del_all_perms(self):
        """
        If the last pfp is removed, shouldn't retain an implicit model
        permission.
        """
        client = Client()
        client.login(username='foo', password='supersekret')
        model_ctype = ContentType.objects.get_for_model(User)
        role_ctype = ContentType.objects.get_for_model(User)
        perms_qs = Permission.objects.filter(
                content_type=model_ctype,
                codename__in=['change_user__is_staff', 'change_user__email'],
                )
        self.user_bar.user_permissions.add(*perms_qs)
        client.post(
                '/admin/perfieldperms/perfieldpermission/manage/',
                data={
                   'pfp_pfilter-model': model_ctype.id,
                   'pfp_pfilter-show_pfps': 'on',
                   'pfp_pfilter-permissions': list(perms_qs.values_list('id', flat=True)),
                   'pfp_pfilter-model_fields': [],
                   'pfp_rfilter-role_type': role_ctype.id,
                   'form-TOTAL_FORMS': '2',
                   'form-INITIAL_FORMS': '2',
                   'form-MIN_NUM_FORMS': '0',
                   'form-MAX_NUM_FORMS': '0',
                   'form-0-role_name': 'foo',
                   'form-0-role_id': '1',
                   'form-0-permissions': [],
                   'form-1-role_name': 'Bar Baa',
                   'form-1-role_id': '2',
                   'form-1-permissions': [],
                   '_save': '',
                },
                follow=True,
                )
        self.assertSetEqual(set(), set(self.user_bar.user_permissions.all()))
        self.assertSetEqual(set(), PFPBackend().get_all_permissions(self.user_bar))

    def test_manage_view_perm_denied(self):
        """Access denied for user without appropriate permission."""
        client = Client()
        client.login(username='bar', password='supersekret')
        response = client.get('/admin/perfieldperms/perfieldpermission/manage/')
        self.assertEqual(response.status_code, 403)

    def test_manage_view_perm_granted(self):
        """User with required permission is granted access."""
        client = Client()
        perm = helpers.get_perm('perfieldperms', 'perfieldpermission', 'manage_permissions')
        self.user_bar.user_permissions.add(perm)
        perm = helpers.get_perm('perfieldperms', 'perfieldpermission', 'add_perfieldpermission')
        self.user_bar.user_permissions.add(perm)
        client.login(username='bar', password='supersekret')
        response = client.get('/admin/perfieldperms/perfieldpermission/manage/')
        self.assertEqual(response.status_code, 200)
