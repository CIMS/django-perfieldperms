from django.test import TestCase
from django.core.exceptions import ValidationError

from perfieldperms.models import PerFieldPermission
from tests import helpers


class TestPerFieldPermission(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_clean(self):
        """
        Exception raised if attempt to create PFP with ContentType different
        from model permission.
        """
        perm1 = helpers.get_perm('auth', 'user', 'change_user')
        perm2 = helpers.get_perm('admin', 'logentry', 'add_logentry')
        pfp = PerFieldPermission(
                content_type=perm2.content_type,
                codename='{}__is_staff'.format(perm1.codename),
                name='{} - is_staff'.format(perm1.name),
                model_permission=perm1,
                field_name='is_staff',
                )
        self.assertRaises(ValidationError, *[pfp.clean])

    def test_validate_unique(self):
        """
        Exception raised if try to create PFP with model_permission and field
        same as existing PFP.
        """
        perm1 = helpers.get_perm('auth', 'user', 'change_user')
        pfp = PerFieldPermission(
                content_type=perm1.content_type,
                codename='{}__is_staff'.format(perm1.codename),
                name='{} - is_staff'.format(perm1.name),
                model_permission=perm1,
                field_name='is_staff',
                )
        pfp.full_clean()
        pfp.save()
        pfp = PerFieldPermission(
                content_type=perm1.content_type,
                codename='{}__is_staff'.format(perm1.codename),
                name='{} - is_staff'.format(perm1.name),
                model_permission=perm1,
                field_name='is_staff',
                )
        self.assertRaises(ValidationError, *[pfp.validate_unique])



