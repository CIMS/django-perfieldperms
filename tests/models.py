from django.db import models
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class NoRelToPermission(models.Model):
    name = models.CharField(max_length=80, blank=False, null=False)


class MultiRelToPermission(models.Model):
    name = models.CharField(max_length=80, blank=False, null=False)
    rel1 = models.ManyToManyField(Permission, related_name='rel1')
    rel2 = models.ManyToManyField(Permission, related_name='rel2')


# Models for testing...?
class PizzaBase(models.Model):
    name = models.CharField(max_length=80)


class Topping(models.Model):
    name = models.CharField(max_length=80)


class Pizza(models.Model):
    name = models.CharField(max_length=80)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    toppings = models.ManyToManyField(Topping, blank=True)
    bases = models.ManyToManyField(PizzaBase, blank=True)


class FieldTester(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField(editable=False)
    pizzas = models.ManyToManyField(Pizza)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=80)
    content_object = GenericForeignKey()


class ChildFieldTester(models.Model):
    parent = models.ForeignKey(FieldTester, on_delete=models.CASCADE, related_name='children')
