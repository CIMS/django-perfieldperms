from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.test import TestCase, override_settings

from perfieldperms.utils import (cache_to_set, get_base_perm, get_unpermitted_fields,
                                 get_usable_model_fields, make_model_perm_sets, make_pfp_perm_sets,
                                 merge_caches)
from . import helpers
from .models import FieldTester, Pizza, Topping


class UtilsTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        pass

    def test_cache_to_set(self):
        """Created appropriate set when given a cache dict()."""
        cache = {
            'foo': {'foo__one', 'foo__two'},
            'bar': set(),
            'baz': {'baz_one'},
            }
        self.assertSetEqual(
                {'foo', 'foo__one', 'foo__two', 'bar', 'baz', 'baz_one'},
                cache_to_set(cache),
                )
        # Test empty cache
        self.assertSetEqual(set(), cache_to_set({}))

    def test_merge_caches_both_empty(self):
        """Empty cache is empty."""
        self.assertDictEqual({}, merge_caches({}, {}))

    def test_merge_caches_to_empty(self):
        """`to_cache` empty effectively returning `from_cache`."""
        to_cache = {}
        from_cache = {
            'hi': set(),
            'hal': set(),
            }
        self.assertDictEqual(
                {'hi': set(), 'hal': set()},
                merge_caches(from_cache, to_cache),
                )

    def test_merge_caches_from_empty(self):
        """`from_cache` empty effectively returning `to_cache`."""
        to_cache = {
            'hi': set(),
            'hal': set(),
            }
        from_cache = {}
        self.assertDictEqual(
                {'hi': set(), 'hal': set()},
                merge_caches(from_cache, to_cache),
                )

    def test_merge_caches_merge_sets(self):
        """Merge sets with same key in caches."""
        to_cache = {
            'hi': {'one'},
            }
        from_cache = {
            'hi': {'two'},
            }
        self.assertDictEqual(
                {'hi': {'one', 'two'}},
                merge_caches(from_cache, to_cache),
                )

    def test_merge_caches_replace_sets(self):
        """
        Empty set in `from_cache` replaces set with same key in `to_cache`.
        """
        to_cache = {
            'hi': {'one', 'two'},
            }
        from_cache = {
            'hi': set(),
            }
        self.assertDictEqual(
                {'hi': set()},
                merge_caches(from_cache, to_cache),
                )

    def test_merge_caches_no_overwrite_empty(self):
        """Empty set in `to_cache` isn't overwritten when `overwrite_empty` = False."""
        to_cache = {'foo': set()}
        from_cache = {'foo': {'bar'}}
        self.assertDictEqual(
                {'foo': set()},
                merge_caches(from_cache, to_cache, overwrite_empty=False),
                )

    def test_make_model_perm_sets_empty(self):
        """No perms = empty dict()."""
        self.assertDictEqual({}, make_model_perm_sets([]))

    def test_make_model_perm_sets(self):
        """Check string formatting in list comprehension is correct. Existing sets overwritten."""
        perms = [('cims_auth', 'add_user'), ('cims_auth', 'change_user')]
        self.assertDictEqual(
                {'cims_auth.add_user': set(), 'cims_auth.change_user': set()},
                make_model_perm_sets(
                        perms,
                        {'cims_auth.change_user': 'cims_auth.change_user__username'}
                        ),
                )

    def test_make_pfp_perm_sets_no_pfps(self):
        """Entries in `perm_sets` should remain if key is not in `pfps`."""
        pfps = []
        perm_sets = {'foo': set(), 'bar': set()}
        self.assertDictEqual(
                {'foo': set(), 'bar': set()},
                make_pfp_perm_sets(pfps, perm_sets),
                )

    def test_make_pfp_perm_sets_update_set(self):
        """Sets in `perm_sets` should be updated with values if entries in
        `pfps` match keys in `perm_sets`."""
        pfps = [
            ('hi', 'foo__one', 'foo'), ('hi', 'foo__two', 'foo'),
            ]
        perm_sets = {'hi.foo': set(), 'bar': set()}
        self.assertDictEqual(
                {'hi.foo': {'hi.foo__one', 'hi.foo__two'}, 'bar': set()},
                make_pfp_perm_sets(pfps, perm_sets),
                )

    def test_make_pfp_perm_sets_create_set(self):
        """Keys in `perm_sets` should be created if required."""
        pfps = [
            ('hi', 'foo__one', 'foo'), ('hi', 'foo__two', 'foo'),
            ]
        perm_sets = {'bar': set()}
        self.assertDictEqual(
                {'hi.foo': {'hi.foo__one', 'hi.foo__two'}, 'bar': set()},
                make_pfp_perm_sets(pfps, perm_sets),
                )

    def test_make_pfp_perm_sets_no_perms_sets(self):
        """Keys in `perm_sets` should be created if required."""
        pfps = [
            ('hi', 'foo__one', 'foo'), ('hi', 'foo__two', 'foo'),
            ]
        self.assertDictEqual(
                {'hi.foo': {'hi.foo__one', 'hi.foo__two'}},
                make_pfp_perm_sets(pfps),
                )

    def test_make_pfp_perm_sets_all_empty(self):
        """No entries in either arg should return empty dict."""
        self.assertDictEqual(
                {},
                make_pfp_perm_sets([]),
                )

    def test_get_base_perm(self):
        """Return the right permission based on state of object passed in."""
        content_type = ContentType.objects.get_for_model(Pizza)
        perm = helpers.get_perm('tests', 'pizza', 'add_pizza')
        # obj is None
        self.assertEqual(perm, get_base_perm(content_type, None))
        # obj hasn't been saved
        pizza = Pizza(name='Margarita', price='10.95')
        self.assertEqual(perm, get_base_perm(content_type, pizza))
        # obj has pk but it's None for some reason
        pizza.pk = None
        self.assertEqual(perm, get_base_perm(content_type, pizza))
        # obj is saved instance so changing
        perm = helpers.get_perm('tests', 'pizza', 'change_pizza')
        pizza = Pizza.objects.create(name='Margarita', price='10.95')
        self.assertEqual(perm, get_base_perm(content_type, pizza))

    def test_get_usable_model_fields(self):
        """
        Shouldn't return not concrete, auto-created, non-editable, or GenericForeignKey fields.

        `id` - concrete auto-created field
        `children` - non-concrete field
        `description` - non-editable field
        `content_object` -  GenericForeignKey field
        """
        fields = get_usable_model_fields(FieldTester)
        self.assertSetEqual({'name', 'content_type', 'pizzas', 'object_id'}, set(fields))


@override_settings(
        AUTHENTICATION_BACKENDS=['perfieldperms.backends.PFPBackend'],
        PFP_MODELS=[('tests', 'fieldtester'), ('tests', 'pizza')],
        PFP_IGNORE_PERMS={'tests': {'pizza': 'add_pizza'}}
        )
class GetUnpermittedFieldsTest(TestCase):
    """
    Shouldn't return not concrete, auto-created, non-editable, or GenericForeignKey fields.
    """

    @classmethod
    def setUpTestData(cls):
        call_command('pfp-makeperms')

    def test_no_permissions(self):
        """User lacks access to anything so will return all possible fields."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        fields = get_unpermitted_fields(FieldTester, user1)
        self.assertSetEqual({'name', 'content_type', 'pizzas', 'object_id'}, set(fields))

    def test_has_permission_for_field(self):
        """Field isn't returned if user has permission."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        pfp = helpers.get_perm('tests', 'fieldtester', 'add_fieldtester__name')
        user1.user_permissions.add(pfp)
        fields = get_unpermitted_fields(FieldTester, user1)
        self.assertNotIn('name', fields)

    def test_change_permissions(self):
        """Check against change permissions if a saved model instance is passed in."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        pfp = helpers.get_perm('tests', 'pizza', 'change_pizza__name')
        user1.user_permissions.add(pfp)
        pizza = Pizza.objects.create(name='Capriciosa', price='13.95')
        fields = get_unpermitted_fields(Pizza, user1, pizza)
        self.assertSetEqual({'price', 'toppings', 'bases'}, set(fields))

    def test_empty_if_no_pfps_configured(self):
        """Blank result if no PFPs exist for this model/action."""
        user1 = User.objects.create_user(username='user1', password='sekret')
        fields = get_unpermitted_fields(Topping, user1)
        self.assertListEqual([], fields)
        fields = get_unpermitted_fields(Pizza, user1)
        self.assertListEqual([], fields)
